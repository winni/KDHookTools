//
//  ZHToastManager.m
//  Pod
//
//  Created by YeQing on 2016/11/16.
//  Copyright © 2016年 naruto. All rights reserved.
//

#import "ZHToastManager.h"
#import "ZHToastKitPrivate.h"
#import "MBProgressHUD+BCHelper.h"

@interface ZHToastManager()
/// 普通文字toast
@property (nonatomic, strong) MBProgressHUD *textToast;
@end

@implementation ZHToastManager
//MARK: - system
static ZHToastManager *kZHToastInstance = nil;
+ (instancetype)shareInstance {
    if (!kZHToastInstance) {
        @synchronized (self) {
            if (!kZHToastInstance) {
                kZHToastInstance = [[ZHToastManager alloc] init];
            }
        }
    }
    return kZHToastInstance;
}

//MARK: - 显示toast
+ (void)showToast:(NSString *)toastStr {
    [self showToast:toastStr offset:CGPointMake(0, 0) toView:nil];
}
+ (void)showToast:(NSString *)toastStr offsetY:(CGFloat)offsetY {
    [self showToast:toastStr offset:CGPointMake(0, offsetY) toView:nil];
}
+ (void)showToast:(NSString *)toastStr offsetX:(CGFloat)offsetX {
    [self showToast:toastStr offset:CGPointMake(offsetX, 0) toView:nil];
}
+ (void)showToast:(NSString *)toastStr offset:(CGPoint)offset {
    [self showToast:toastStr offset:offset toView:nil];
}
+ (void)showToastOnBottom:(NSString *)toastStr {
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    [self showToast:toastStr offset:CGPointMake(0, height/3) toView:nil];
}
+ (void)showToast:(NSString *)toastStr toView:(UIView *)view {
    [self showToast:toastStr offset:CGPointMake(0, 0) toView:view];
}
+ (void)showToast:(NSString *)toastStr offset:(CGPoint)offset toView:(UIView *)view {
    if (view == nil){
        view = [[UIApplication sharedApplication].delegate window];
    }
    MBProgressHUD *hud = nil;
//    if (ZHToastManager.shareInstance.textToast) {
//        //有缓存toast
//        hud = ZHToastManager.shareInstance.textToast;
//        if (!hud.superview) {
//            [view addSubview:hud];
//        }
//        else if (hud.superview && hud.superview != view) {
//            //已经存在，父view不一致，重新添加
//            [hud removeFromSuperview];
//            [view addSubview:hud];
//        }
//        //更新text
//        hud.detailsLabel.text = toastStr;
//        //修改位置
//        hud.offset  = CGPointMake(0, offsetY);
//        hud.hidden = false;
//        //手动显示
//        [hud showAnimated:YES];
//    } else {
        //没有缓存toast
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.mode = MBProgressHUDModeText;
        hud.bctoast = YES;
        hud.bezelView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.6f];
        hud.bezelView.layer.cornerRadius = 3;
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.detailsLabel.font = [UIFont systemFontOfSize:14];
        hud.detailsLabel.textColor = [UIColor whiteColor];
        hud.removeFromSuperViewOnHide = YES;
        //更新text
        hud.detailsLabel.text = toastStr;
        //修改位置
        hud.offset  = offset;
//        //缓存
//        ZHToastManager.shareInstance.textToast = hud;
//    }
    [hud hideAnimated:YES afterDelay:1.5f];
}

+ (void)showError:(NSString *)error toView:(UIView *)view
{
    [self show:error icon:ZHToastBundleImage(@"mb_error") view:view afterDelay:1.5f];
}
+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:ZHToastBundleImage(@"mb_success") view:view afterDelay:1.5f];
}
+ (void)show:(NSString*)text icon:(UIImage *)icon view:(UIView*)view afterDelay:(NSTimeInterval)delay {
    if (view == nil){
        view = [[UIApplication sharedApplication].delegate window];
    }
    [self.class hideHud:view];
    // 快速显示一个提示信息
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bctoast = YES;
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc] initWithImage:icon];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7f];
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.detailsLabel.text = text;
    hud.detailsLabel.font = [UIFont systemFontOfSize:14];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:delay];
}

#pragma mark - 显示提示框 ProgressHUD
+ (MBProgressHUD *)showHud:(NSString *)message {
   return [[self class] showHud:message toView:nil offset:CGPointMake(0, 0)];
}
+ (MBProgressHUD *)showHud:(NSString *)message offsetX:(CGFloat)offsetX {
    return [[self class] showHud:message toView:nil offset:CGPointMake(offsetX, 0)];
}
+ (MBProgressHUD *)showHud:(NSString *)message offsetY:(CGFloat)offsetY {
    return [[self class] showHud:message toView:nil offset:CGPointMake(0, offsetY)];
}
+ (MBProgressHUD *)showHud:(NSString *)message offset:(CGPoint)offset {
    return [[self class] showHud:message toView:nil offset:offset];
}
+ (MBProgressHUD *)showHud:(NSString *)message toView:(UIView *)view {
    return [[self class] showHud:message toView:view offset:CGPointMake(0, 0)];
}
+ (MBProgressHUD *)showHud:(NSString *)message toView:(UIView *)view offset:(CGPoint)offset {
    if (view == nil){
        view = [[UIApplication sharedApplication].delegate window];
    }
    [self.class hideHud:view];
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.offset = offset;
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4f];
    hud.bezelView.layer.cornerRadius = 12.f;
    hud.contentColor = [UIColor whiteColor];
    hud.label.textColor = [UIColor whiteColor];
    hud.label.text = message;
    hud.removeFromSuperViewOnHide = YES;
    return hud;
}

#pragma mark - 隐藏提示框 ProgressHUD
+ (void)hideHud
{
    [[self class] hideHud:nil];
}
+ (void)hideHud:(UIView *)view
{
    if (view == nil){
        view = [[UIApplication sharedApplication].delegate window];
    }
    [MBProgressHUD bc_hideHUDForView:view animated:YES];
}
+ (void)hideHud:(UIView *)view afterDelay:(NSTimeInterval)delay
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.class hideHud:view];
    });
}

#pragma mark - 是否已有提示框

+ (BOOL)isHudAppeared {
    return [self isHudAppearedInView:nil];
}

+ (BOOL)isHudAppearedInView:(UIView *)view {
    if (view == nil){
        view = [[UIApplication sharedApplication].delegate window];
    }
    
    __block MBProgressHUD *subHudView = nil;
    [view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[MBProgressHUD class]]) {
            MBProgressHUD *subHudViewTmp = (MBProgressHUD *)obj;
            if(!subHudViewTmp.bctoast && !subHudViewTmp.hidden){
                subHudView = subHudViewTmp;
                *stop = YES;
            }
        }
    }];
    if (subHudView != nil) {
        return YES;
    }
    return NO;
}

@end
