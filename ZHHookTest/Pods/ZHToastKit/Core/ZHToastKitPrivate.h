//
//  ZHToastKitPrivate.h
//  Pods
//
//  Created by YeQing on 2017/3/10.
//  toast 提示组件 定义
//

#ifndef ZHToastKitPrivate_h
#define ZHToastKitPrivate_h

#pragma mark - 读片读取

#define    ZHToastBundleName                        @"ZHToastKit.bundle"
#define    ZHToastBundleImage(name)                  ([UIImage imageNamed:name])?:(([UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",ZHToastBundleName,name]])?:[UIImage imageNamed:[NSString stringWithFormat:@"Frameworks/%@.framework/%@/%@",@"ZHToastKit",ZHToastBundleName,name]])

#endif /* ZHToastKitPrivate_h */
