//
//  MBProgressHUD+BCHelper.h
//  Pods
//
//  Created by YeQing on 2017/3/28.
//
//  MBProgressHUD category


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"
#import "MBProgressHUD.h"
#pragma clang diagnostic pop

@interface MBProgressHUD (BCHelper)
/// 是否是toast
@property (nonatomic, assign) BOOL                              bctoast;


#pragma mark - 隐藏 view 上的 hud view
/**
 隐藏 view上的hud view

 @param view 父view
 @param animated 动画参数
 @return BOOL
 */
+ (BOOL )bc_hideHUDForView:(UIView *)view animated:(BOOL)animated;


#pragma mark - 隐藏 view 上的 toast view

/**
 隐藏 view上的toast view
 
 @param view 父view
 @param animated 动画参数
 @return BOOL
 */
+ (BOOL )bc_hideToastForView:(UIView *)view animated:(BOOL)animated;
@end
