//
//  MBProgressHUD+BCHelper.m
//  Pods
//
//  Created by YeQing on 2017/3/28.
//
//

#import "MBProgressHUD+BCHelper.h"
#import <objc/runtime.h>

@implementation MBProgressHUD (BCHelper)

#pragma mark - bctoast
-(BOOL)bctoast
{
    return [objc_getAssociatedObject(self, @selector(bctoast)) boolValue];
    
}
-(void)setBctoast:(BOOL)bctoast
{
    objc_setAssociatedObject(self, @selector(bctoast), @(bctoast), OBJC_ASSOCIATION_ASSIGN);
}

#pragma mark - 隐藏 view 上的 hud view(非toast view)
+ (BOOL)bc_hideHUDForView:(UIView *)view animated:(BOOL)animated
{
    __block MBProgressHUD *subHudView = nil;
    [view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[MBProgressHUD class]]) {
            MBProgressHUD *subHudViewTmp = (MBProgressHUD *)obj;
            if(!subHudViewTmp.bctoast){
                subHudView = subHudViewTmp;
                *stop = YES;
            }
        }
    }];
    if (subHudView != nil) {
        subHudView.removeFromSuperViewOnHide = YES;
        [subHudView hideAnimated:animated];
        return YES;
    }
    return NO;
}



#pragma mark - 隐藏 view 上的 toast view
+ (BOOL )bc_hideToastForView:(UIView *)view animated:(BOOL)animated
{
    __block MBProgressHUD *subToastView = nil;
    [view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[MBProgressHUD class]]) {
            MBProgressHUD *subToastViewTmp = (MBProgressHUD *)obj;
            if(subToastViewTmp.bctoast){
                subToastView = subToastViewTmp;
                *stop = YES;
            }
        }
    }];
    if (subToastView != nil) {
        subToastView.removeFromSuperViewOnHide = YES;
        [subToastView hideAnimated:animated];
        return YES;
    }
    return NO;
}


@end
