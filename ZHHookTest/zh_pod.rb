#!/usr/bin/ruby

#1.远程branch+源码集成
##clone仓库到本地目录(group/组件）,使用 :path 生成 Development Pods
##适合组件开发阶段，边修改，边提交。

#2.远程branch+lib集成
##clone仓库到本地目录(group/组件）,使用 :podspec 指定 lib.podsepc,引用编译好的library

#3.远程tag、commit+源码集成
##使用cocoapod，下载源代码，生成Pods

#4.远程tag、commit+lib集成
##下载tag、commit代码到本地目录(group+Lib/组件）,使用 :podspec 指定 lib.podsepc,引用编译好的library
##适合比较稳定不经常改动组件，减少编译时间

#5.本地仓库+源码集成
##使用 :path 生成 Development Pods
##适合新开发的组件，没有远程仓库

#6.本地仓库+lib集成
##使用 :podspec 指定 lib.podsepc,引用编译好的library
##适合新开发的组件，没有远程仓库


#flutter pod
##pName: pods name
#flutter_release: flutter plugin 模式。debug还是release
##use_remote: 是否使用远程仓库的代码，0:使用本地;1:使用branch;2:使用tag;;3:使用commit
##sub_specs: 子pod 列表
def bc_flutter (pName, pGitInfo)
    bc_pod(pName, pGitInfo)
end


def bc_pod (pName, pGitInfo)
    #name
    if pName.include? "/"
        names = pName.split("/")
        pName = names[0]
        sub_module = names[1]
    end
    git = pGitInfo[:git];#git url
    tag = pGitInfo[:tag];#git tag
    #release
    flutter_release = pGitInfo[:flutter_release];
    #git commit
    commit = pGitInfo[:commit];
    #git 分支
    branch = pGitInfo[:branch];
    if branch.nil?
        branch = ""
    end
    #使用使用远程仓库
    use_remote = pGitInfo[:use_remote];
    if use_remote.nil? || use_remote.blank?
        use_remote = 0
    end
    #sub spec
    sub_specs = pGitInfo[:sub_specs];
    if sub_specs.nil? && !flutter_release.nil?
        sub_specs = Array.new
    end
    if !flutter_release.nil? && flutter_release < 1
        sub_specs << "debug"
    elsif !flutter_release.nil? && flutter_release >= 1
        sub_specs << "release"
    end
    #同一个仓库下的子模块
    if sub_module.blank?
        sub_module = pGitInfo[:sub_module];#sub module
    end
    if sub_module.blank?
        sub_module = pName
    end
    #是否使用编译后的lib
    use_lib = pGitInfo[:use_lib];#是否使用lib
    if(use_lib.nil? || use_lib.blank?)
        use_lib = 0 #默认不适用lib,使用源码方式
    end
    #获取repo group
    bc_dirRegex=/\:.*\//;
    bc_MatchData=bc_dirRegex.match(git);
    com_name=bc_MatchData[0];
    if(!com_name.blank?)
        com_name[0]="";
        com_name[-1]="";
    else
        #使用默认目录
        com_name="ios-component";
    end
    #开发模式的临时目录、本地repo目录
    tmp_dir = pGitInfo[:tmp_dir];
    com_dir = pGitInfo[:com_dir];
    if (com_dir.nil? || com_dir.blank?)
        com_dir="../component/";
    end
    com_path = com_dir+com_name
    
    #使用本地代码
    if(use_remote == 0)
        #使用本地仓库
        _tmp_dir = ""
        if tmp_dir.nil? or tmp_dir.empty?
            _tmp_dir = com_path
        else
            _tmp_dir = tmp_dir
        end
        if(use_lib<=0)
            #不使用lib，源码编译
            pod sub_module, :subspecs => sub_specs, :path => "#{_tmp_dir}/#{pName}/", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        else
            #使用编译后的lib
            pod sub_module, :podspec => "#{_tmp_dir}/#{pName}/Library/Library.podspec", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        end
        return
    end
    
    #使用远程仓库代码，branch方式，develop pod模式
    if(use_remote == 1 && branch.present?)
        system "source ./zh_component_loader.sh #{git} && load_component_with_branch #{git} #{branch} #{sub_module} #{com_path}"
        if(use_lib<=0)
            #不使用lib，源码编译
            pod sub_module, :subspecs => sub_specs, :path => "#{com_path}/#{pName}/", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        else
            #使用编译后的lib
            pod sub_module, :podspec => "#{com_path}/#{pName}/Library/Library.podspec", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        end
        return
    end
    
    #使用远程仓库代码，tag方式
    if(use_remote == 2 && tag.present?)
        #tag模式
        if (use_lib<=0)
            #不使用lib，源码编译
            pod sub_module, :subspecs => sub_specs, :git => git, :tag => tag, :inhibit_warnings => pGitInfo[:inhibit_warnings]
        else
            #使用编译后的lib
            com_path = com_path+"-Lib"
            system "source ./zh_component_loader.sh #{git} && load_component_with_tagcommit #{tag} \"\"#{com_path}"
            pod sub_module, :podspec => "#{com_path}/#{pName}/Library/Library.podspec", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        end
        return
    end
    
    #使用远程仓库代码，commit方式
    if(use_remote == 3 && commit.present?)
        #commit模式
        if (use_lib<=0)
            #不使用lib，源码编译
            if(!sub_specs.blank?)
                pod sub_module, :subspecs => sub_specs, :git => git, :commit => commit, :inhibit_warnings => pGitInfo[:inhibit_warnings]
            else
                pod sub_module, :git => git, :commit => commit, :inhibit_warnings => pGitInfo[:inhibit_warnings]
            end
        else
            #使用编译后的lib
            com_path_tmp = com_path+"-Lib"
            system "source ./zh_component_loader.sh #{git} && load_component_with_tagcommit #{git} \"\"#{commit} #{com_path_tmp}"
            pod sub_module, :podspec => "#{com_path_tmp}/#{pName}/Library/Library.podspec", :inhibit_warnings => pGitInfo[:inhibit_warnings]
        end
        return
    end
    puts "pod error:#{pName},#{use_remote}"
end
