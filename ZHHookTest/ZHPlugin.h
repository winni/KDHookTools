//
//  ZHPlugin.h
//  ZHPlugin
//
//  Created by YeQing on 2021/1/22.
//

#import <Foundation/Foundation.h>

@interface ZHPlugin : NSObject
/// 获取单例
+ (ZHPlugin *)shared;
/// 开始执行
- (void)start;
@end
