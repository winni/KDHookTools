//
//  AppDelegate.h
//  ZHHookTest
//
//  Created by YeQing on 2021/1/21.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, strong) UIWindow * window;
@end

