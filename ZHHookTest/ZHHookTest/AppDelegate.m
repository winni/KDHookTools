//
//  AppDelegate.m
//  ZHHookTest
//
//  Created by YeQing on 2021/1/21.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    ViewController *rootVC = [[ViewController alloc] init];
    self.window.rootViewController = rootVC;
    return YES;
}
@end
