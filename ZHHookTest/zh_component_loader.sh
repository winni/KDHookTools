#!/bin/bash
################################
#根据 tag、commit、branch 下载组件

#根据 branch 下载、更新组件
load_component_with_branch() {
    #解析参数
    bc_giturl=$1
    bc_branch=$2
    bc_comDir=$4
    if [ ! $bc_comDir ];then
        #没有这个参数，自己截取目录
        bc_comDir=`echo ${bc_branch} | grep -o '\:.*\/'`
        bc_comDir=`echo ${bc_comDir:1:(${#bc_comDir}-2)}`
        #所有基础组件都在这个目录
        if [ ! $bc_comDir ];then
            #默认放在基础组件目录
            bc_comDir="ios-component"
        fi
    fi
    #1.参数验证
    if [ ! $bc_giturl ];then
        echo   "error:git url参数 为空"
        return 
    fi
    ##2.配置 基础Pod目录
    #cd ../
    #创建 pod的基础目录
    if [ ! -d "$bc_comDir" ]
    then
        mkdir -p "$bc_comDir"
    fi
    cd $bc_comDir
    if [ $? -ne 0 ]; then
        echo "config pod dir error"
        return
    fi
    #3.配置 具体的Pod
    gitName=${bc_giturl##*/}
    podName=${gitName%.git}
    localGit=${PWD}/${podName}/.git
    echo "\033[34m \r\n* 开始配置 $podName \033[0m"
    if [ ! -d "$localGit" ]; then
        echo "start git clone..."
        git clone $bc_giturl
        if [ $bc_branch ];then
            curDir=${PWD}
            cd "${PWD}/${podName}/"
            git fetch
            git checkout $bc_branch
            cd "${curDir}"
        fi
        if [ $? -ne 0 ]; then
            echo "ZHUpdate install error"
            return
        fi
    else
        echo "start git pull..."
        cd "${PWD}/${podName}/"
        #        git reset --hard && git clean -dfx
        if [ $bc_branch ];then
            git fetch
            git checkout $bc_branch
            git pull
        else
            git pull
        fi
        
        if [ $? -ne 0 ]; then
            cd "${curDir}"
            echo "ZHUpdate install error"
            return
        else
            cd "${curDir}"
        fi
    fi
}


#根据tag、commit下载代码
load_component_with_tagcommit() {
    #解析参数
    bc_giturl=$1
    bc_tag=$2
    bc_commit=$3
    bc_comDir=$4
    #1.参数验证
    if [ ! $bc_giturl ];then
        echo "config pod tag,git url empty"
        return
    fi
    #2.配置 基础Pod目录
    cd ../
    #创建 pod的基础目录
    if [ ! -d "$bc_comDir" ]
    then
        mkdir -p "$bc_comDir"
    fi

    if [ $? -ne 0 ]; then
        echo "config pod tag,dir error"
        return
    fi
    cd $bc_comDir
    if [ $? -ne 0 ]; then
        echo "config pod tag,dir error"
        return
    fi
    #重新clone
    gitName=${bc_giturl##*/}
    podName=${gitName%.git}
    #删除以前下载 tag、commit 代码
    rm -rf ${podName}

    if [ $bc_tag ];then
        echo "\033[34m \r\n* 开始配置 $podName,$bc_tag \033[0m"
        git clone --branch $bc_tag $bc_giturl
    else
        echo "\033[34m \r\n* 开始配置 $podName,$bc_commit \033[0m"
        git checkout $bc_commit
    fi
    if [ $? -ne 0 ]; then
        echo "配置组件失败"
        return
    fi
    cd "${curDir}"
}


