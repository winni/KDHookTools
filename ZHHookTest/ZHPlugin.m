//
//  ZHPlugin.m
//  ZHPlugin
//
//  Created by YeQing on 2021/1/22.
//

#import "ZHPlugin.h"
#import <UIKit/UIKit.h>
//#import <BCFoundation/BCSwizzle.h>
//#import <ZHToastKit/ZHToastKit.h>

@interface ZHPlugin()

@end

@implementation ZHPlugin

//MARK: - life cycle
static ZHPlugin *ZHPluginShareInstance = nil;
+ (void)load {
    [[ZHPlugin shared] start];
}
+ (ZHPlugin *)shared {
    @synchronized (self) {
        if(!ZHPluginShareInstance){
            ZHPluginShareInstance = [[ZHPlugin alloc] init];
        }
    }
    return ZHPluginShareInstance;
}

//MARK: - 开始
- (void)start {
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [ZHToastManager showToast:@"hook"];
//    });
//    BCSwizzleInstanceMethod([UIViewController class], BCHookSelector(@"viewDidLoad"), BCSWReturnType(void), BCSWArguments(), BCSWReplacement({
//        [selfObj bczh_viewDidLoad];
//        BCSWCallOriginal();
//    }));
}


//MARK: - hook
//-(void)setZh_enableSwizzle:(BOOL)zh_enableSwizzle {
//    objc_setAssociatedObject(self, @selector(zh_enableSwizzle), @(zh_enableSwizzle), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}
//-(BOOL)zh_enableSwizzle {
//    id enableSwizzle = objc_getAssociatedObject(self, @selector(zh_enableSwizzle));
//    if (enableSwizzle == nil) {
//        //判断是否可以swizzle
//        BOOL zh_canSwizle = YES;
//        NSString *selfClass = NSStringFromClass([self class]);
//        NSArray *classList = [selfClass componentsSeparatedByString:@"."];
//        if (classList.count>1) {
//            //兼容swift class: target.XXxxVC
//            selfClass = classList.lastObject;
//        }
//        if([selfClass hasPrefix:@"UI"] || [selfClass hasPrefix:@"_UI"]  || [selfClass hasPrefix:@"_SFAppPassword"] || [selfClass hasPrefix:@"SFPassword"]  || [selfClass hasPrefix:@"CKSMS"] || [selfClass hasPrefix:@"CAM"] || [selfClass hasPrefix:@"PUUI"] || [selfClass hasPrefix:@"PUPhoto"] || [selfClass hasPrefix:@"PLPhoto"] || [selfClass hasPrefix:@"GS"] || [selfClass hasPrefix:@"Vod"] || [selfClass hasPrefix:@"AVPlayer"] || [selfClass hasPrefix:@"AVFullScreen"]){
//            //CAM 开头：拍照库 ，PUUI：相册库，PLPhoto：相册编辑，CKSMS：短信page
//            //GS,Vod 互动课堂SDK
//            zh_canSwizle = NO;
//        }
//        else if([self isKindOfClass:[UITabBarController class]]) {
//            zh_canSwizle = NO;
//        }
//        else if([self isKindOfClass:[UINavigationController class]]) {
//            zh_canSwizle = NO;
//        }
//        self.zh_enableSwizzle = zh_canSwizle;
//        return zh_canSwizle;
//    }
//    return [enableSwizzle boolValue];
//}
//- (void)bczh_viewDidLoad {
//    if (!self.zh_enableSwizzle) {
//        return;
//    }
//    NSLog(@"[hook] %@ load",NSStringFromClass([self class]));
//}
@end
