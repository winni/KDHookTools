//
//  ZHPlugin.h
//  ZHPlugin
//
//  Created by YeQing on 2021/1/22.
//

#import <Foundation/Foundation.h>

//! Project version number for ZHPlugin.
FOUNDATION_EXPORT double ZHPluginVersionNumber;

//! Project version string for ZHPlugin.
FOUNDATION_EXPORT const unsigned char ZHPluginVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZHPlugin/PublicHeader.h>

@interface ZHPlugin : NSObject
/// 获取单例
+ (ZHPlugin *)shared;
/// 开始执行
- (void)start;
@end
