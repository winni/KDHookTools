//
//  UIImage+BCGIF.h
//  BCFoundation
//
//  Created by chun.chen on 2019/12/16.
//  gif 图片加载

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (BCGIF)

/// 返回gif动画图片
/// @param name gif图片名称
+ (UIImage *)bc_animatedGIFNamed:(NSString *)name;

/// GIF data转化image
/// @param data data description
+ (UIImage *)bc_animatedGIFWithData:(NSData *)data;

/// GiF图片适应尺寸
/// @param size size description
- (UIImage *)bc_animatedImageByScalingAndCroppingToSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
