#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "BCFoundation.h"
#import "BCFoundationUtils.h"
#import "BCUUIDUtil.h"
#import "NSArray+BCHelper.h"
#import "NSAttributedString+BCHelper.h"
#import "NSDate+BCHelper.h"
#import "NSDictionary+BCHelper.h"
#import "NSMutableAttributedString+BCHelper.h"
#import "NSNull+BCHelper.h"
#import "NSNumber+BCHelper.h"
#import "NSObject+BCBlock.h"
#import "NSString+BCHelper.h"
#import "NSURL+BCHelper.h"
#import "BCSwizzle.h"
#import "RSSwizzle.h"
#import "UIColor+BCHelper.h"
#import "UIDevice+BCHardware.h"
#import "UIImage+BCGIF.h"
#import "UIImage+BCHelper.h"
#import "BCFileHelper.h"
#import "BCOperation.h"
#import "BCOperationQueue.h"

FOUNDATION_EXPORT double BCFoundationVersionNumber;
FOUNDATION_EXPORT const unsigned char BCFoundationVersionString[];

