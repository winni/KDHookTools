//
//  ZHToastManager.h
//  Pod
//
//  Created by YeQing on 2016/11/16.
//  Copyright © 2016年 naruto. All rights reserved.
//  提示浮层库，
//  1.文字toast共享，只会创建一次
//  2.loading toast目前不共享，每次创建一个view

#import <Foundation/Foundation.h>
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wstrict-prototypes"
#import "MBProgressHUD.h"
#pragma clang diagnostic pop

NS_ASSUME_NONNULL_BEGIN

@interface ZHToastManager : NSObject
//MARK: - system
/// 共享单例
+ (instancetype)shareInstance;

#pragma mark - 显示toast
+ (void)showToast:(NSString * _Nullable)toastStr;
+ (void)showToast:(NSString * _Nullable)toastStr offsetY:(CGFloat )offsetY;
+ (void)showToast:(NSString * _Nullable)toastStr offsetX:(CGFloat )offsetX;
+ (void)showToast:(NSString * _Nullable)toastStr offset:(CGPoint )offset;
+ (void)showToastOnBottom:(NSString * _Nullable)toastStr;
+ (void)showToast:(NSString * _Nullable)toastStr toView:(UIView * _Nullable)view;
+ (void)showError:(NSString * _Nullable)error toView:(UIView * _Nullable)view;
+ (void)showSuccess:(NSString * _Nullable)success toView:(UIView * _Nullable)view;


#pragma mark -  显示提示框 ProgressHUD

/**
 显示 hud 提示 [window 上]

 @param message 消息内容
 @return MBProgressHUD
 */
+ (MBProgressHUD *)showHud:(NSString * _Nullable)message;
+ (MBProgressHUD *)showHud:(NSString * _Nullable)message offsetX:(CGFloat )offsetX;
+ (MBProgressHUD *)showHud:(NSString * _Nullable)message offsetY:(CGFloat )offsetY;
+ (MBProgressHUD *)showHud:(NSString * _Nullable)message offset:(CGPoint )offset;
+ (MBProgressHUD *)showHud:(NSString * _Nullable)message toView:(UIView * _Nullable)view;

#pragma mark - 隐藏提示框 ProgressHUD

/**
 隐藏window上面的提示框
 */
+ (void)hideHud;
+ (void)hideHud:(UIView * _Nullable)view;
+ (void)hideHud:(UIView * _Nullable)view afterDelay:(NSTimeInterval)delay;

#pragma mark - 是否已有提示框 ProgressHUD
+ (BOOL)isHudAppeared;
+ (BOOL)isHudAppearedInView:(UIView * _Nullable)view;
@end

NS_ASSUME_NONNULL_END
