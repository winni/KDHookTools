# 简介

 iOS 逆向工程，frida一键砸壳，插入framework，重签名打包ipa。
 
 
 **1. 砸壳**
 
 使用`./file/frida-ios-dump-3.x`在越狱手机上砸壳。
 
 **2. 准备插件**
 
 `ZHHookTest`为`hook`插件工程。
 
 **3. 注入插件**
 
 * 动态注入`./file/ZHPlugin.framework`，并且拷贝`./file/copy-frameworks`相关依赖库。

 
 * 使用`./file.embedded.mobileprovision`重签名所有可执行文件和`frameworks`。

 
 * 最后输出注入后的`ipa`安装包。